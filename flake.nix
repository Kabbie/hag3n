{
  description = "A very basic flake";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    };

    outputs = { self, nixpkgs }:

        let 
            system = "x86_64-linux";

            pkgs = nixpkgs.packages.${system};
        

        in {
            i18n = {
                defaultLocale = "en_US.UTF-8";
                supportedLocales = [ "en_US.UTF-8/UTF-8" ];
            };  
            console = {
                font = "Lat2-Terminus16";
                keyMap = "us";
            };
            fonts = {
                fontDir.enable = true;
                enableGhostscriptFonts = true;
                fonts = with pkgs; [
                    powerline-fonts
                    nerdfonts
                ];
            };
        };
}
